package fr.univsavoie.etu;


import java.util.*;

/**
 * 
 */
public class Ticket extends Observable {

	
    public Vector<Plat> listePlats;
    EtatTicket etat;
    //String type;

    
    
    /**
     * Default constructor
     */
    public Ticket(Vector<Plat> plat) {
    	listePlats = plat;
    	etat = new EtatTicketEnAttente(this);
    	
    }

    /**
     * 
     */

    public String toString(){
    	return "Ticket info :\n\t "+"Liste plat :"+listePlats.toString()+"\n\t Etat : "+etat;
    }

    /**
     * @param Etat
     */
    public void changeEtatTicket(String etat0) {
        //etat = etat0;
    }
    
    public void setEtatTicket(EtatTicket e){
    	this.notifyObservers();
    	this.etat = e;
    }
    
    /*
     * Methodes observable/observer
     */
    public void notifierObservateurs()
    {
            setChanged();// M�thode de l'API.
           // notifyObservers();// Egalement une m�thode de l'API.
            etat.notifierObservateurs();
    }

	public void aEnvoyer() {
		etat.aEnvoyer();

	}

	public void aPreparer() {
		// TODO Auto-generated method stub
		etat.aPreparer();
	}

	public void aVenirChercher() {
		// TODO Auto-generated method stub
		etat.aVenirChercher();
	}

	public void aRecuperer() {
		// TODO Auto-generated method stub
		etat.aRecuperer();

	}
	
	public boolean estBar(){
		return etat.estBar();
	}
	public boolean estCuisine(){
		return etat.estCuisine();
	}
    

}