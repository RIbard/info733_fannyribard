package fr.univsavoie.etu;

public interface EtatTicket {
	
	public void aEnvoyer();
	public void aPreparer();
	public void aVenirChercher();
	public void aRecuperer();
	public void notifierObservateurs();
	public boolean estBar();
	public boolean estCuisine();
	public String toString();

}
