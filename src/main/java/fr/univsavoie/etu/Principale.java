package fr.univsavoie.etu;

import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.Vector;

public class Principale {


public static void main(String[] args) {
		
		Vector<Ticket> notifTicket = new Vector();
		Serveur serveur_courant;
		Restaurant bonneFranquette = new Restaurant();
    	bonneFranquette.initRestaurant("ee");
    	connexion(bonneFranquette);
    	
		
	}

	private static void accueilClient(Restaurant bonneFranquette, Serveur serveur) {
	// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int nb_client;
		System.out.print("Nombre de client :");
		nb_client = obtenirChoix();
		
		//System.out.print("Table :");	
		Vector<Table> listeTable = serveur.listeTable;
		System.out.println("Tables affect�es : ");
		for (int i = 0; i < listeTable.size(); i++) {
			System.out.println("\t- Table n�"+i+", statut : "+listeTable.get(i).etat);
		}		
		System.out.print("\nTable :");
		int table = obtenirChoix();
		while (listeTable.get(table).etat == "Occupe"){
			
			System.out.print("\nTable non disponible, choisissez une autre table : ");
			System.out.print("\nTable :");
			table = obtenirChoix();
		}

		Table t0 = bonneFranquette.assignerTable(nb_client,table);
		Commande c = bonneFranquette.creerCommande(t0);
		
		System.out.println("\n1- Ajouter un element � la commande");
		System.out.println("2- Ajouter un ticket � la commande");
		System.out.println("3- Envoyer prochain ticket");
		System.out.println("0- Terminer la cr�ation de la commande");
		
		System.out.print("Choix :");
		int t = sc.nextInt();
		
		while (t != 0){
			switch (t) {
			case 1:
				ajouteElementCommande(bonneFranquette,c);
				break;
			case 2:
				creerTicketCommande(bonneFranquette,c);
				break;
			case 3:
				c.envoyerPlatSuivant();
				break;
			default:
				break;
			}
			System.out.println("\n1- Ajouter un element � la commande");
			System.out.println("2- Ajouter un ticket � la commande");
			System.out.println("3- Envoyer prochain ticket");
			System.out.println("0- Terminer la cr�ation de la commande");
			
			System.out.print("Choix :");
			t = obtenirChoix();
		}
		
		
		
		
		
		
	
}

	private static void creerTicketCommande(Restaurant bonneFranquette, Commande c) {
		Vector<Plat> plats = c.getListePlatsAPreparer();
		Vector<Plat> listeRes = new Vector();
		int choix = 0;
		
		while(!plats.isEmpty()){
			afficheListePlat(plats);
			System.out.println("-1 - Cr�er le ticket");
			System.out.print("\nPlat � ajouter : ");
			choix = obtenirChoix();
			
			if (choix == -1){
				System.out.println("Ticket cr��\n");
				c.creerTicket(listeRes);
				listeRes = new Vector();
			}
			else{
				listeRes.add(plats.get(choix));			
				plats.remove(choix);
			}
			
		}
		
		c.creerTicket(listeRes);
		c.etat = EtatCommande.Prete;
		
	}


	static void ajouteElementCommande(Restaurant bonneFranquette, Commande c) {
		int choix;
		
		afficheCategorieCarte();
		
		choix = obtenirChoix();
		
		int qte;


		
		switch (choix) {
		case 1:
			Vector<Menu> listeMenu = bonneFranquette.maCarte.getListeMenu();
			afficheListeMenu(listeMenu);
			System.out.print("Choix :");
			choix = obtenirChoix();
			System.out.print("Quantite :");
			qte = obtenirChoix();
			c.ajoutMenu(listeMenu.get(choix), qte);
			
			break;
		case 2:
			System.out.print("Quantite :");
			qte = obtenirChoix();
			c.ajoutMenu(bonneFranquette.maCarte.menuDuJour,qte);
			break;
		case 3:
			
			afficheMenuCategoriePlat();
			choix = obtenirChoix();
			Vector<Plat> listePlat = new Vector();
			
			switch (choix) {
			case 1:
				listePlat = bonneFranquette.maCarte.getListeEntree();
				afficheListePlat(listePlat);
				break;
			case 2:
				listePlat = bonneFranquette.maCarte.getListeRepas();
				afficheListePlat(listePlat);
				break;
			case 3:
				listePlat = bonneFranquette.maCarte.getListeDessert();
				afficheListePlat(listePlat);
				break;
			case 4:
				listePlat = bonneFranquette.maCarte.getListeBoisson();
				afficheListePlat(listePlat);
				break;
			case 5:
				listePlat = bonneFranquette.maCarte.getListePlats();
				afficheListePlat(listePlat);
				break;

			default:
				break;
			}
			
			System.out.print("\nChoix :");
			choix = obtenirChoix();
			System.out.print("Quantite :");
			qte = obtenirChoix();
			c.ajoutPlat(listePlat.get(choix), qte);
			//System.out.println("Ajout Plat : " + listePlat.get(choix).nom);			
			break;

		default:
			break;
		}
		
		
		
	}

	private static void afficheCategorieCarte() {
		System.out.println("\nChoisir une cat�gorie :");
		System.out.println("1- Menus");
		System.out.println("2- Menu du jour");
		System.out.println("3- Plats");
		System.out.println("0- Quitter");
		
		System.out.print("\nChoix : ");
		
	}

	public static void choixConsulterCarte(Restaurant r){

		int choix = 0;
		
		afficheCategorieCarte();
		choix = obtenirChoix();
		
		switch (choix) {
		case 1:
			Vector<Menu> allMenu = r.consulterMenus();
			System.out.println("Liste de menus : ");
			for (int i = 0; i < allMenu.size(); i++) {
				System.out.println(i + " : " + allMenu.get(i).afficheMenus());
			}
			
			System.out.print("\nMenu � afficher : ");
			
			Scanner ch3 = new Scanner(System.in);
			int choix_menu = ch3.nextInt();

			System.out.println(allMenu.get(choix_menu).afficheMenuDetaille() + "\n");
			
			
			break;
		case 2:
			System.out.println(r.consulterMenuDuJour() + "\n");
			break;
		case 3:
			choixPlat(r);
			break;
		default:
			break;
		}
		
	}
	
	
	public static void choixPlat(Restaurant r) {
		int choix;
		
		afficheMenuCategoriePlat();
		choix = obtenirChoix();
		
		switch (choix) {
		case 1:
			System.out.println(r.consulterPlat("Entree")+"\n");
			break;
		case 2:
			System.out.println(r.consulterPlat("Plat")+"\n");
			break;
		case 3:
			System.out.println(r.consulterPlat("Dessert")+"\n");
			break;
		case 4:
			System.out.println(r.consulterPlat("Boisson")+"\n");
			break;
		case 5:
			System.out.println(r.consulterPlat("Tout")+"\n");
			break;
		default:
			break;
		}
		
	}

	private static void afficheMenuCategoriePlat() {
		System.out.println("\nListe de plats � afficher :");
		System.out.println("1- Entrees");
		System.out.println("2- Plat principal");
		System.out.println("3- Dessert");
		System.out.println("4- Boisson");
		System.out.println("5- Tout");
		
		System.out.print("\nVotre choix : ");
		
	}

	private static void consulterCommande(Restaurant bonneFranquette, Serveur serveur) {
		Vector<Table> listeTable;
		Commande c;
		Vector<Ticket> listeTicket;

		listeTable = serveur.listeTable;
		
		for (int i = 0; i < listeTable.size(); i++) {
			System.out.println("\t- Table n�"+i+", statut : "+listeTable.get(i).etat);
		}		
		System.out.print("\nTable :");
		int table = obtenirChoix();
		while (listeTable.get(table).etat == "libre"){
			System.out.print("\nTable libre, choisissez une autre table : ");
			System.out.print("\nTable :");
			table = obtenirChoix();
		}
		
		c = listeTable.get(table).getCommande();
		
		System.out.println("\nAction sur commande");
		System.out.println("1- Ajouter un ticket");
		System.out.println("2- Afficher liste des tickets");
		System.out.println("3- Afficher ticket courant");
		System.out.println("4- Details commande");
		System.out.println("5- Envoyer prochain ticket");
		System.out.println("0- Quitter");
		
		System.out.print("\nChoix :");
		int choix = obtenirChoix();
		
		while (choix != 0){
		try
		{
		switch (choix) {
		case 2:
			System.out.println("Liste des tickets");
			System.out.println(c.getListeTicket().toString());
			break;
		case 3:
			System.out.println("\n Ticket courant");
			System.out.println(c.ticketCourant.toString());
			break;
		case 4:
			System.out.println("Detail commande");
			System.out.println("--Liste Menu--");
			afficheListeMenu(c.listeMenu);
			System.out.println("\n--Liste Plat--");
			afficheListePlat(c.listeCommandeAPayer);
			break;
		case 5:
			c.envoyerPlatSuivant();
			break;
		case 1:
			creerTicketCommande(bonneFranquette,c);
			break;
		default:
			break;
		}
		}
		catch(NullPointerException e){
			System.out.println("Liste vide");
		}
		System.out.println("\nAction sur commande");
		System.out.println("1- Ajouter un ticket");
		System.out.println("2- Afficher liste des tickets");
		System.out.println("3- Afficher ticket courant");
		System.out.println("4- Details commande");
		System.out.println("5- Envoyer prochain ticket");
		System.out.println("0- Quitter");
		
		System.out.print("Choix :");
		choix = obtenirChoix();
		}
		
		
	}
	
	static void afficheListeMenu(Vector<Menu> liste){
		for (int i = 0; i < liste.size(); i++) {
			System.out.println(i+"- "+liste.get(i).nom);
		}
	}

	static void afficheListePlat(Vector<Plat> liste){
		for (int i = 0; i < liste.size(); i++) {
			System.out.println(i+"- "+liste.get(i).nom);
		}
	}
	
	static void afficheListeTicket(Vector<Ticket> liste){
		for (int i = 0; i < liste.size(); i++) {
			System.out.println(i+"- "+liste.get(i));
		}
	}
	
	static int obtenirChoix(){
		Scanner sc = new Scanner(System.in);
		return sc.nextInt();
	}
	
	static void connexion(Restaurant bonneFranquette){
		System.out.println("\n###### A LA BONNE FRANQUETTE #####");
		System.out.println("Connexion: ");
		Vector<Serveur> listeServeur = new Vector();
		listeServeur = bonneFranquette.afficheServeur();
		for (int i = 0; i < listeServeur.size(); i++) {
			System.out.println(i+"-"+listeServeur.get(i).nom);
		}
		System.out.print("Choix:");
		int choix = obtenirChoix();
		//return listeServeur.get(choix);
		Serveur serveur_courant = listeServeur.get(choix);
		while (true){
					
					//serveur_courant = connexion(bonneFranquette);
					switch (serveur_courant.poste) {
					case "Serveur":
						menuServeur(bonneFranquette,serveur_courant);
						break;
					case "Cuisinier":
						menuPersonnelNonSalle(bonneFranquette,serveur_courant);
						break;
					case "Barman":
						menuPersonnelNonSalle(bonneFranquette,serveur_courant);
						break;
					case "Patron":
						menuPatron(bonneFranquette,serveur_courant);
						break;
					default:
						break;
					}
				}
	}
	
	private static void menuPatron(Restaurant bonneFranquette, Serveur serveur_courant) {
		int choix_main = -1;
		while (choix_main != 0){

			System.out.println("\n###### A LA BONNE FRANQUETTE #####");
			System.out.println("1- Effectuer payement");
			System.out.println("0- Quitter");

			System.out.print("Choix : ");
			
			choix_main = obtenirChoix();
			
			int choix;
		
			switch (choix_main) {
			case 1:
				menuPayement(bonneFranquette);
				break;
			case 0:
				connexion(bonneFranquette);
				break;
			default:
				break;				
			}
		}
		
		
	}

	private static void menuPayement(Restaurant bonneFranquette) {
		Vector<Table> listeTable;
		Commande c;
		Vector<Ticket> listeTicket;

		listeTable = bonneFranquette.listeTable;
		
		for (int i = 0; i < listeTable.size(); i++) {
			System.out.println("\t- Table n�"+i+", statut : "+listeTable.get(i).etat);
		}		
		System.out.print("\nTable :");
		int table = obtenirChoix();
		while (listeTable.get(table).etat == "libre"){
			System.out.print("\nTable libre, choisissez une autre table : ");
			System.out.print("\nTable (-1 pour quitter) :");
			table = obtenirChoix();
			if (table == -1) {
				menuPatron(bonneFranquette, null);
			}
		}
		c = listeTable.get(table).getCommande();
		
		System.out.println("\nAction sur commande");
		System.out.println("1- R�gler commande");
		System.out.println("0- Quitter");
		
		System.out.print("\nChoix :");
		int choix = obtenirChoix();
		
		switch (choix) {
		case 1:
			reglerCommande(c,listeTable.get(table));
			break;
		default:
			break;
		}
	
	}

	private static void reglerCommande(Commande c,Table t) {
		if (c.etat == EtatCommande.EnAttentePayement) {

		System.out.println("Recu :");
		if (!c.listeMenu.isEmpty()) {
			for (Menu m : c.listeMenu) {
				System.out.println("- "+m.nom+" : "+m.prix+" �");
			}
		}
		if (!c.listeCommandeAPayer.isEmpty()) {
			for (Plat p : c.listeCommandeAPayer) {
				System.out.println("- "+p.nom+" : "+p.prix+" �");
			}
		}
		System.out.println("Somma � r�gler : "+c.calculPrix()+" �");
		System.out.println("Payement r�alis� :");
		System.out.println("1) Oui");
		System.out.println("2) Non");
		int choix = obtenirChoix();
		if (choix == 1) {
			c.etat = EtatCommande.Reglee;
			t.etat = "libre";
			t.c = null;
			
		}	
		
		}
		else {
			System.out.println("Le repas doit etre terminer pour payer");
		}
		
		
	}

	private static void menuPersonnelNonSalle(Restaurant bonneFranquette, Serveur serveur_courant) {
		int choix_main = -1;
		while (choix_main != 0){

			System.out.println("\n###### A LA BONNE FRANQUETTE #####");
			System.out.println("1- Consulter la carte");
			System.out.println("2- Mes tickets en cours");
			System.out.println("0- Quitter");

			System.out.print("Choix : ");
			
			choix_main = obtenirChoix();
			
			int choix;
		
			switch (choix_main) {
			case 1:
				choixConsulterCarte(bonneFranquette);
				break;
			case 2:
				notifTicket(bonneFranquette,serveur_courant);
				break;
			case 0:
				connexion(bonneFranquette);
				break;
			default:
				break;
				
			}
		}
		
	}

	static void menuServeur(Restaurant bonneFranquette, Serveur serveur){
		int choix_main = -1;
		while (choix_main != 0){

			System.out.println("\n###### A LA BONNE FRANQUETTE #####");
			System.out.println("1- Consulter la carte");
			System.out.println("2- Consulter une commande");
			System.out.println("3- Creer commande");
			System.out.println("4- Mes tickets en cours");
			System.out.println("0- Quitter");

			System.out.print("Choix : ");
			
			choix_main = obtenirChoix();
			
			int choix;
		
			switch (choix_main) {
			case 1:
				choixConsulterCarte(bonneFranquette);
				break;
			case 2:
				consulterCommande(bonneFranquette,serveur);
				break;
				
			case 3:
				accueilClient(bonneFranquette,serveur);
				break;
			case 4:
				notifTicket(bonneFranquette,serveur);
				break;
			case 0:
				connexion(bonneFranquette);
				break;
			default:
				break;
				
			}
		}

	}

	private static void notificationTicket(Restaurant bonneFranquette, Serveur serveur) {
		Vector<Ticket> ticketsEnCours = new Vector();
		
//		for (int i = 0; i < serveur.listeTable.size(); i++) {		
//			if (serveur.listeTable.get(i).c != null){
//				Vector<Ticket> tmp = serveur.listeTable.get(i).c.listeTicket;
//				for (int j = 0; j < tmp.size(); j++) {

//					if (tmp.get(j).etat == "en pr�paration"){
//						System.out.println("Ticket de la table n�"+ i +" , statut : "+tmp.get(j).etat);
//					}
//				}
//			}
//		}

	
	}
	
	private static void notifTicket(Restaurant resto, Serveur serveur){
		switch (serveur.poste) {
		case "Barman":
			notifTicketBarman(resto, serveur);
			break;
		case "Cuisinier":
			notifTicketCuisinier(resto, serveur);
			break;
		case "Serveur":
			notifTicketServeur(resto, serveur);
			break;
		case "Patron":
	
			break;
		default:
			break;
		}
	}

	private static void notifTicketBarman(Restaurant resto, Serveur serveur) {
		int choix;
		System.out.println(serveur.nom);
		Vector<Ticket> listeTicketEstEnvoye = new Vector();
		Vector<Ticket> listeTicketEnPreparation = new Vector();
		
		for (Table t : resto.listeTable) {
			if (t.c != null) {
				for (Ticket ticket : t.c.listeTicket) {
					if (ticket.estBar()) {
						if (ticket.etat instanceof EtatTicketEstEnvoye) {
							listeTicketEstEnvoye.addElement(ticket);
						}
						if (ticket.etat instanceof EtatTicketEnPreparation) {
							listeTicketEnPreparation.addElement(ticket);
						}
					}
						
				}
			}
		}
		System.out.println("\n##Ticket � prendre en charge## ");
		afficheListeTicket(listeTicketEstEnvoye);
		
		System.out.println("\n##Ticket � envoyer##");
		afficheListeTicket(listeTicketEnPreparation);
		
		//menuActionTicket(resto,serveur);
		
		System.out.println("\n1) Prendre en charge un ticket");
		System.out.println("2) Envoy� un ticket");
		System.out.println("0) Quitter");
		System.out.print("\n Choix: ");
		choix = obtenirChoix();
		
		switch (choix) {
		case 1:
			afficheListeTicket(listeTicketEstEnvoye);
			System.out.print("Choix: ");
			choix = obtenirChoix();
			listeTicketEstEnvoye.get(choix).aPreparer();
			break;
		case 2:
			afficheListeTicket(listeTicketEnPreparation);
			System.out.print("Choix: ");
			choix = obtenirChoix();
			listeTicketEnPreparation.get(choix).aVenirChercher();
			break;
		case 0:
			break;
		default:
			break;
		}
		
		
	}
//
//	private static void menuActionTicket(Restaurant resto, Serveur serveur) {
//		int choix = obtenirChoix();
//		
//	}

	private static void notifTicketCuisinier(Restaurant resto, Serveur serveur) {
		int choix;
		System.out.println(serveur.nom);
		Vector<Ticket> listeTicketEstEnvoye = new Vector();
		Vector<Ticket> listeTicketEnPreparation = new Vector();
		
		for (Table t : resto.listeTable) {
			if (t.c != null) {
				for (Ticket ticket : t.c.listeTicket) {
					if (ticket.estCuisine()) {
						if (ticket.etat instanceof EtatTicketEstEnvoye) {
							listeTicketEstEnvoye.addElement(ticket);
						}
						if (ticket.etat instanceof EtatTicketEnPreparation) {
							listeTicketEnPreparation.addElement(ticket);
						}
					}
						
				}
			}
		}
		System.out.println("\n##Ticket � prendre en charge## ");
		afficheListeTicket(listeTicketEstEnvoye);
		
		System.out.println("\n##Ticket � envoyer##");
		afficheListeTicket(listeTicketEnPreparation);
		
		//menuActionTicket(resto,serveur);
		
		System.out.println("\n1) Prendre en charge un ticket");
		System.out.println("2) Envoy� un ticket");
		System.out.println("0) Quitter");
		System.out.print("\n Choix: ");
		choix = obtenirChoix();
		
		switch (choix) {
		case 1:
			afficheListeTicket(listeTicketEstEnvoye);
			System.out.print("Choix: ");
			choix = obtenirChoix();
			listeTicketEstEnvoye.get(choix).aPreparer();
			break;
		case 2:
			afficheListeTicket(listeTicketEnPreparation);
			System.out.print("Choix: ");
			choix = obtenirChoix();
			listeTicketEnPreparation.get(choix).aVenirChercher();
			break;
		case 0:
			break;
		default:
			break;
		}
		
	}

	private static void notifTicketServeur(Restaurant resto, Serveur serveur){
		
		//System.out.println(serveur.listeTable);
		System.out.println(serveur.nom);
		Vector<Ticket> listeTicket = new Vector();
		Vector<Table> listeTable = new Vector();
		
		for (Table t : serveur.listeTable) {
			if (t.c != null) {
				for (Ticket ticket : t.c.listeTicket){
					if (ticket.etat instanceof EtatTicketAVenirChercher) {
						listeTicket.addElement(ticket);
						listeTable.add(t);
						//System.out.println(ticket);
					}
				}
				//System.out.println(t.c.listeTicket);
			}
		}
		
		Vector<Ticket> listeTicketBar = new Vector();
		Vector<Ticket> listeTicketCuisine = new Vector();
		for (Ticket ticket : listeTicket) {
			if (ticket.estBar()) {
				listeTicketBar.addElement(ticket);
			}
			if (ticket.estCuisine()){
				listeTicketCuisine.addElement(ticket);
			}
		}

		if (!listeTicketBar.isEmpty()) {
			System.out.println("Ticket � r�cup�rer au bar");
			afficheListeTicket(listeTicketBar);
		}
		else {
			System.out.println("Pas de tickets � r�cup�rer au bar");
		}
		
		if (!listeTicketCuisine.isEmpty()) {
			System.out.println("Ticket � r�cup�rer en Cuisine");
			afficheListeTicket(listeTicketCuisine);
		}else {
			System.out.println("Pas de tickets � r�cuperer en cuisine");
		}
		
	}
	
}
